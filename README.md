# README #

### Keppler theme for Shopify ###

Initial, pre-release version

### How do I get set up? ###

Use Gulp for SCSS compilation:

`npm install -g gulp`

`npm install gulp-cssimport`

`gulp watch`

Use Shopify’s Themekit for syncing your local files:

`brew install theme kit`

`theme configure --password=5474f48eebbb1210490f74ee0878196e --store=cafe-keppler-koffie.myshopify.com --themeid=176165636`

`theme watch`

### Contact ###

Casper Schipper
casperschipper@yahoo.com